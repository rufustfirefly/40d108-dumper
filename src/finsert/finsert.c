#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>


int main(int argc, char *argv[]){

  FILE *in, *in2;
  FILE *out;
  int i, n;
	int offset;
	char val, val2;
  
  if (argc != 5) {
    printf("Usage: finsert f1_name f1_hex_offset ins_name outfile\n");
    return -1;
  }
  
  if ((in = fopen(argv[1], "rb")) == NULL) {
    printf("Cant't open file name %s\n", argv[1]);
    return -1;
  }
  if ((in2 = fopen(argv[3], "rb")) == NULL) {
    printf("Cant't open file name %s\n", argv[3]);
    return -1;
  }
  
  if ((out = fopen(argv[4], "wb")) == NULL) {
    printf("Cant't open file name %s\n", argv[4]);
    fclose(in);
    return -1;
  }

	sscanf(argv[2],"%x",&offset);
  
  for(i=0;!feof(in);i++) {

		n=fread(&val,sizeof(val),1,in);

		if(i>=offset && !feof(in2)){
			fread(&val,sizeof(val),1,in2);
		}

		if(n>0){
	    fwrite(&val, sizeof(val),1, out);
		}
  }

  fclose(out);
  fclose(in);
  fclose(in2);

  return 0;
}
