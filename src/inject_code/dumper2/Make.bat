
set path=f:\_dev\chdk_gcc\;f:\_dev\chdk_gcc\bin;f:\_tools;f:\_dev\tools
del *.bin

cls
arm-elf-gcc -fno-builtin -O2 -Ilib -nostdinc -c entry.s
arm-elf-gcc -fno-builtin -O1 -Ilib -c main.c
arm-elf-gcc -fno-builtin -O2 -Ilib -nostdlib -Wl,-N,-Ttext,00807214 -o main.exec entry.o main.o -lc -lgcc

arm-elf-objcopy -O binary main.exec ../../dumper2.bin

del *.o main.exec


