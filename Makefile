# make-dumper.sh
# bitbucket.org/rufustfirefly
# 
# Make firmware dumpers from 40D108 loader

all: clean prep tools dissect decrypt inject encrypt assemble checksum

clean:
	rm -rf tmp 108D?.fir

tools:
	make -C src/1D_5D_40D_30D_400D_check_sum && cp -v src/1D_5D_40D_30D_400D_check_sum/eos_fsum ./eos_fsum
	make -C src/decrypt_40D108_flasher && cp -v src/decrypt_40D108_flasher/decrypt_40D108_flasher ./decrypt_40D108_flasher
	make -C src/dissect_fw3 && cp -v src/dissect_fw3/dissect_fw3 ./dissect_fw3
	make -C src/finsert && cp -v src/finsert/finsert finsert.bin
	make -C src/inject_code/dumper1
	make -C src/inject_code/dumper2

prep:
	mkdir -p tmp

dissect:
	# Dissect firmware into constituent components
	./dissect_fw3 ../40d00108.fir tmp 40D108

decrypt:
	# Decrypt the flasher
	./decrypt_40D108_flasher tmp/40D108_1_flasher.bin tmp/40D108_1_flasher.bin.dec

inject:
	# Inject the new code (dumper 1 and dumper 2)
	./finsert.bin tmp/40D108_1_flasher.bin.dec 70F4 dumper1.bin tmp/40D108_1_flasher.bin.D1.dec
	./finsert.bin tmp/40D108_1_flasher.bin.dec 70F4 dumper2.bin tmp/40D108_1_flasher.bin.D2.dec

encrypt:
	# Recrypt
	./decrypt_40D108_flasher tmp/40D108_1_flasher.bin.D1.dec tmp/40D108_1_flasher.bin.D1
	./decrypt_40D108_flasher tmp/40D108_1_flasher.bin.D2.dec tmp/40D108_1_flasher.bin.D2

assemble:
	# Assemble
	cat \
	       	tmp/40D108_0_header.bin \
		tmp/40D108_1_flasher.bin.D1 \
	       	tmp/40D108_2_data_head.bin \
		tmp/40D108_3_data_body.bin \
		> 108D1.fir
	cat \
	       	tmp/40D108_0_header.bin \
		tmp/40D108_1_flasher.bin.D2 \
	       	tmp/40D108_2_data_head.bin \
		tmp/40D108_3_data_body.bin \
		> 108D2.fir

checksum:
	# Adjusting checksums
	./eos_fsum 108D1.fir
	./eos_fsum 108D2.fir

cleanup:
	# Cleanup after build
	rm -rf tmp

