# 40D108-DUMPER

This is a collection of pieces from the CHDK forums which should build two
firmware dumpers, launched from the 40D 1.0.8 firmware update.

The binaries here are compiled for Linux 64-bit, although they can be
recompiled for other platforms.

It assumes an install of a cross compilation toolkit in
``/opt/gcc-arm-none-eabi-4_7-2013q2/`` from Launchpad.

